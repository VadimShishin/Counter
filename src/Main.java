import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Counter counter = new Counter();
        counter.load();

        System.out.println("Вы запустили консольное приложение Счётчик");
        System.out.println("Текущее значение счётчика: " + counter.getValue());

        Scanner scanner = new Scanner(System.in);

        boolean running = true;
        while (running) {
            System.out.print("Введите команду: ");
            String input = scanner.nextLine();

            switch (input) {
                case "/inc" -> {
                    counter.increment();
                    System.out.println("Новое значение счётчика: " + counter.getValue());
                }
                case "/stop" -> {
                    counter.save();
                    System.out.println("Текущее значение счётчика: " + counter.getValue());
                    System.out.println("Завершаю работу");
                    running = false;
                }
                case "/reset" -> {
                    counter.reset();
                    System.out.println("Счётчик обнулён");
                }
                default -> System.out.println("Неверная команда");
            }
        }
        scanner.close();
    }
}
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class Counter {
    private int value;
    private final Path savePath;

    public Counter() {
        this.value = 0;
        this.savePath = Path.of("counter.txt");
    }

    public int getValue() {
        return value;
    }

    public void increment() {
        value++;
    }

    public void reset() {
        value = 0;
    }

    public void save() {
        try {
            Files.writeString(savePath, String.valueOf(value), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            System.out.println("Ошибка при сохранении счётчика");
        }
    }

    public void load() {
        try {
            if (Files.exists(savePath)) {
                String valueString = Files.readString(savePath);
                value = Integer.parseInt(valueString);
                System.out.println("Счётчик загружен, значение " + value);
            }
        } catch (IOException | NumberFormatException e) {
            System.out.println("Ошибка при загрузке счётчика");
        }
    }
}